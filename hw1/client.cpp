#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
int main(int argc, char *argv[])
{
    char buffer[300], request[300], action[100], trash[100];
    char *token;
    int sockfd;
    int n, i, num;
    int remain_hands[4], num_remain_hands = 0;
    int port;
    int ans;
    int bet, times, in_range;
    int money = 2000;
    int leave, legal = 1;
    char op[10];
    struct sockaddr_in serverAddress;
    while (1) {
        leave = 0;
        printf("[GAME]\nyou have %d dollars.\nBlack Jack or Guess number or leave(b/g/l)? ", money);
        scanf("%s", op);
        if (op[0] == 'l')
            break;
        else if (op[0] == 'b')
            port = atoi(argv[2]);
        else if (op[0] == 'g')
            port = atoi(argv[3]);
        if (op[0] == 'b')
            printf("connect to server 1\n");
        else
            printf("connect to server 2\n");
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        bzero(&serverAddress, sizeof(serverAddress));
        serverAddress.sin_family = AF_INET;
        inet_pton(AF_INET, argv[1], &serverAddress.sin_addr);
        serverAddress.sin_port = htons(port);
        if (connect(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
            printf("connection fail\n");
            continue;
        }
        if (op[0] == 'g') {//guess number
            printf("GUESS NUMBERS\n");
            while (!leave) {
                if (legal) {
                    bzero(buffer, sizeof(buffer));
                    n = read(sockfd, buffer, sizeof(buffer));
                    if (n == 0 || n == -1) {
                        leave = 1;
                        close(sockfd);
                        continue;
                    }
                }
                printf("%s", buffer);
                fflush(stdout);
                if (buffer[0] == 'W') {//welcome
                    legal = 1;
                    scanf("%s", request);
                } else if (buffer[0] == 'p') {//next game
                    legal = 1;
                    leave = 1;
                    close(sockfd);
                    continue;
                } else if (buffer[0] == 'g' || buffer[0] == 'r') {//guess number starts or round n
                    legal = 1;
                    scanf("%d", &ans);
                    sprintf(request, "guess:%04d", ans);
                } else if (buffer[0] == 'c' || buffer[0] == 't') {//correct or too many rounds
                    legal = 1;
                    sprintf(request, "finished\n");
                } else if (buffer[0] == 'w') {
                    legal = 1;
                    money += 100;
                    printf("your current money:%d\nback to main menu\n", money);
                    leave = 1;
                    close(sockfd);
                    continue;
                } else if (buffer[0] == 'l') {
                    legal = 1;
                    money -= 100;
                    printf("your current money:%d\nback to main menu\n", money);
                    leave = 1;
                    close(sockfd);
                    continue;
                }
                if (request[0] == 'r') {
                    printf("loading...\n");
                    write(sockfd, request, strlen(request));
                } else if (request[0] == 'g' || request[0] == 'f')
                    write(sockfd, request, strlen(request));
                else {
                    printf("ilegal msg\n");
                    legal = 0;
                }
            }
        } else {//black jack
            printf("BLACK JACK\n");
            in_range = 0;
            while (!in_range) {
                printf("bet: ");
                scanf("%d", &bet);
                if (bet <= money)
                    in_range = 1;
            }
            while (!leave) {
                if (legal) {
                    bzero(buffer, sizeof(buffer));
                    n = read(sockfd, buffer, sizeof(buffer));
                    if (n == 0 || n == -1) {
                        leave = 1;
                        close(sockfd);
                        continue;
                    }
                }
                printf("%s", buffer);
                fflush(stdout);
                if (buffer[0] == 'W') {//welcome
                    legal = 1;
                    scanf("%s", request);
                } else if (buffer[0] == 'p') {//next game
                    legal = 1;
                    leave = 1;
                    close(sockfd);
                    continue;
                } else if (buffer[0] == 'y') {//broadcast
                    legal = 1;
                    scanf("%s", action);
                    sprintf(request, "action:(%s)\n", action);
                } else if (buffer[0] == 'r') {// remaining hands, status
                    legal = 1;
                    sscanf(buffer, "remaining hands(%d):", &num_remain_hands);
                    //token until status line
                    for (i = 0; i < 4; i++)
                        remain_hands[i] = 0;
                    for (i = 0, token = buffer + strlen("remaining hands(0):"); i < num_remain_hands; i++, token += 2) {
                        sscanf(token, "%d", &n);
                        remain_hands[n] = 1;
                    }
                    token = strtok(buffer, "\n");
                    token = strtok(NULL, "\n");
                    sscanf(token, "status(%d hand):%s", &num, trash);
                    sprintf(request, "action:(");
                    for (i = 0; i < num; i++) {
                        if (remain_hands[i]) {
                        printf("action of hand %d: ", i);
                        fflush(stdout);
                        scanf("%s", action);
                        strcat(request, action);
                        } else {
                           strcat(request, "ov");// hands that already over
                        }
                        if (i != num - 1)
                            strcat(request, "-");
                    }
                    strcat(request, ")");
                } else if (buffer[0] == 'h') {
                    legal = 1;
                    sprintf(request, "finished\n");
                    printf("wait until others finished.\n");
                } else if (buffer[0] == 'd') { 
                    legal = 1;
                    token = strtok(buffer, "\n");
                    token = strtok(NULL, "\n");
                    if (token[0] == 'w') {
                        sscanf(token, "win %d times.\n", &times);
                        money += bet * times;
                        printf("your current money:%d\nback to main menu\n", money);
                    } else if (token[0] == 'l') {
                        money -= bet;
                        printf("your current money:%d\nback to main menu\n", money);
                    } else {
                        printf("your current money:%d\nback to main menu\n", money);
                    }
                    leave = 1;
                    close(sockfd);
                    continue;
                }
                if (request[0] == 'r') {
                    printf("loading...\n");
                    write(sockfd, request, strlen(request));
                } else if (request[0] == 'a' || request[0] == 'f')
                    write(sockfd, request, strlen(request));
                else {
                    printf("ilegal msg\n");
                    legal = 0;
                }
            }
        }
    }
    return 0;
}
