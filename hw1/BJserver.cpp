#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <algorithm>

#define MAX_PLAYER 3

int cards[52];

using namespace std;

int deal(void)
{
    static int n = 0;
    int card;
    card = cards[n] % 13 + 1;
    n = (n + 1) % 52;
    return card;
}

typedef struct hand
{
    int cards[20];
    int num_cards;
    int num_a;
    int bj;
    int stand;
    int is_double;
    hand(void) {
        num_cards = 0;
        num_a = 0;
        bj = 0;
        stand = 0;
        is_double = 0;
    }
    void clear(void) { 
        num_cards = 0;
        num_a = 0;
        bj = 0;
        stand = 0;
        is_double = 0;
    }
    void add_card(int x) {
        cards[num_cards] = x;
        num_cards++;
        if (x == 1)
            num_a++;
        if (num_a > 0 && num_cards == 2)
            bj = 1;
        return;
    }
    int get_score(void) {
        int i, score = 0;
        for (i = 0; i < num_cards; i++) {
            if (cards[i] >= 10) 
                score += 10;
            else 
                score += cards[i];
        }
        if (num_a > 0) {
            if (score + 10 <= 21)
                score += 10;
        }
        return score;
    }
    int can_split(void) {
        if (num_cards == 2) {
            if (cards[0] >= 10 && cards[1] >= 10)
                return 1;
            else if (cards[0] == cards[1])
                return 1;
            else 
                return 0;
        } else
            return 0;
    }
    int can_double(void) {
        if (num_cards == 2)
            return 1;
        else 
            return 0;
    }
}hand;

typedef struct player
{
    hand hands[4];
    int num_hands;
    int num_over;
    player(void) {
        num_hands = 1;
        num_over = 0;
    }
    void clear(void) {
        int i;
        for (i = 0; i < 4; i++)
            hands[i].clear();
        num_hands = 1;
        num_over = 0;
        return;
    }
    void add(int x, int idx) {//add card x
        hands[idx].add_card(x);
        return;
    }
    void do_dealer_job(void) {
        while (hands[0].get_score() < 17)
            hands[0].add_card(deal());
    }
    void split(int idx) {
        int card;
        if (num_hands < 4) {
            card = hands[idx].cards[1];
            hands[idx].num_cards--;
            hands[idx].add_card(deal());
            hands[num_hands].add_card(card);
            hands[num_hands].add_card(deal());
            num_hands++;
        }
        return;
    }
    int win_times(player *d) {
        int i;
        int times = 0;
        int d_score, p_score;
        d_score = d->hands[0].get_score();
        for (i = 0; i < num_hands; i++) {
            p_score = hands[i].get_score();
            if (hands[i].is_double) {
                if (p_score > 21)
                    times -= 2;
                else if (d_score > 21 && p_score <= 21)
                    times += 2;
                else if (d_score > p_score)
                    times -= 2;
                else if (d->hands[0].bj && !hands[i].bj)
                    times -= 2;
                else if (!d->hands[0].bj && hands[i].bj)
                    times += 2;
                else if (d_score < p_score)
                    times += 2;
            } else {
                if (p_score > 21)
                    times -= 1;
                else if (d_score > 21 && p_score <= 21)
                    times += 1;
                else if (d_score > p_score)
                    times -= 1;
                else if (d->hands[0].bj && !hands[i].bj)
                    times -= 1;
                else if (!d->hands[0].bj && hands[i].bj)
                    times += 1;
                else if (d_score < p_score)
                    times += 1;
            }
        }
        return times;
    }
}player;

void get_broad_msg(char msg[], player p[], player* d, int n, int num_player, char num_to_card[][5])
{
    int i;
    char buffer[100];
    sprintf(msg, "you are player %d\n", n);
    for (i = 0; i < num_player; i++) {
        sprintf(buffer, "player %d:", i);
        strcat(msg, buffer);
        if (i != n) {
            sprintf(buffer, "%s ?\n", num_to_card[p[i].hands[0].cards[0]]);
            strcat(msg, buffer);
        } else {
            sprintf(buffer, "%s %s\n", num_to_card[p[i].hands[0].cards[0]], num_to_card[p[i].hands[0].cards[1]]);
            strcat(msg, buffer);
        }
    }
    strcat(msg, "dealer:");
    sprintf(buffer, "%s ?\n", num_to_card[d->hands[0].cards[0]]);
    strcat(msg, buffer);
    if (p[n].hands[0].can_split())
        strcat(msg, "action:hit me or stand or double or split(hi/st/do/sp) ");
    else 
        strcat(msg, "action:hit me or stand or double(hi/st/do) ");
    return;
}

void get_player_msg(char msg[], player p[], player* d, int n, int num_player, char num_to_card[][5])
{
    int i, j;
    char buffer[300];
    msg[0] = '\0';
    if (p[n].num_over == p[n].num_hands) {
        sprintf(buffer, "have no action to do, wait until others finished\n");
        strcat(msg, buffer);
    } else {
        sprintf(buffer, "remaining hands(%d):", p[n].num_hands - p[n].num_over);
        strcat(msg, buffer);
    }
    for (i = 0; i < p[n].num_hands; i++) {
        if (!(p[n].hands[i].get_score() > 21 || p[n].hands[i].stand || p[n].hands[i].is_double)) {
            sprintf(buffer, "%d ", i);
            strcat(msg, buffer);
        }
    }
    strcat(msg, "\n");
    sprintf(buffer, "status(%d hand):\n", p[n].num_hands);
    strcat(msg, buffer);
    for (i = 0; i < p[n].num_hands; i++) {
        sprintf(buffer, "hand %d(", i);
        strcat(msg, buffer);
        if (p[n].hands[i].get_score() > 21) {
            strcat(msg, "bust");
        } else if (p[n].hands[i].stand) {
            strcat(msg, "stand");
        } else if (p[n].hands[i].is_double) {
            strcat(msg, "double");
        }
        else {
            if (p[n].hands[i].can_split() && p[n].num_hands < 4)
                strcat(msg, "sp/");
            if (p[n].hands[i].can_double())
                strcat(msg, "do/");
            strcat(msg, "st/");
            strcat(msg, "hi");
        }
        strcat(msg, "): ");
        for (j = 0; j < p[n].hands[i].num_cards; j++) {
            sprintf(buffer, "%s ", num_to_card[p[n].hands[i].cards[j]]);
            strcat(msg, buffer);
        }
        strcat(msg, "\n");
    }
    sprintf(buffer, "dealer:%s ?\n", num_to_card[d->hands[0].cards[0]]);
    strcat(msg, buffer);
    return;
}

int main(int argc, char *argv[])
{
    char buffer[300], msg[300], cmd[300], tmp_msg[300];
    int listen_fd, conn_fd, max_fd;
    int i, fd;
    int n, hand_idx;
    int times;
    int num_ready = 0, num_conn = 0, num_finish = 0;
    int game_played = 0;
    char num_to_card[14][5];
    char *token;
    player p[MAX_PLAYER], dealer;
    int fd_num_table[20], num_fd_table[MAX_PLAYER];
    fd_set active_fds, read_fds;
    socklen_t length;
    struct sockaddr_in serverAddress, clientAddress;
    //set socket params
    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(atoi(argv[1]));
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(listen_fd, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
    //listen
    listen(listen_fd, 10);
    //init fd_set
    FD_ZERO(&active_fds);
    FD_SET(listen_fd, &active_fds);
    max_fd = listen_fd;
    //init cards
    for (i = 0; i < 52; i++)
        cards[i] = i;
    srand(time(0));
    random_shuffle(cards, cards + 52);
    //create num to card table
    strcpy(num_to_card[1], "A");
    for (i = 2; i <= 10; i++) {
        sprintf(buffer, "%d", i);
        strcpy(num_to_card[i], buffer);
    }
    strcpy(num_to_card[11], "J");
    strcpy(num_to_card[12], "Q");
    strcpy(num_to_card[13], "K");
    while(1) {
        read_fds = active_fds;
        select(max_fd + 1, &read_fds, NULL, NULL, NULL);
        for (fd = 3; fd <= max_fd; fd++) {
            if (FD_ISSET(fd, &read_fds)) {
                //new connection
                if (fd == listen_fd) {
                    length = sizeof(clientAddress);
                    conn_fd = accept(listen_fd, (struct sockaddr*)&clientAddress, &length);
                    //the room is playing
                    if (game_played || num_conn == MAX_PLAYER) {
                        sprintf(msg, "please wait for the next game\n");
                        write(conn_fd, msg, strlen(msg));
                        close(conn_fd);
                        continue;
                    }    
                    if (conn_fd == -1)
                        perror("accept");
                    else {
                        FD_SET(conn_fd, &active_fds);
                        fd_num_table[conn_fd] = num_conn;
                        num_fd_table[num_conn] = conn_fd;
                        printf("no.%d player entered\n", num_conn);
                        num_conn++;
                        if (conn_fd > max_fd)
                            max_fd = conn_fd;
                        sprintf(msg, "Welcome to black jack, enter r when you are ready. ");
                        write(conn_fd, msg, strlen(msg));
                    }
                } 
                //other connections
                else {
                    bzero(&buffer, sizeof(buffer));
                    read(fd, buffer, sizeof(buffer));
                    printf("buffer:%s\n", buffer);
                    if (buffer[0] == 'r') { //ready
                        //init params
                        printf("no.%d player is ready\n", fd_num_table[fd]);
                        num_ready++;
                        n = fd_num_table[fd];
                        //give player n 2 cards
                        p[n].add(deal(), 0);
                        p[n].add(deal(), 0);
                        //when everyone is ready
                        if (num_ready == num_conn) {
                            game_played = 1;
                            num_finish = 0;
                            //dealer get 2 cards
                            dealer.add(deal(), 0);
                            dealer.add(deal(), 0);
                            //sent game start msg to everyone
                            printf("game starts\n");
                            for (i = 0; i < num_ready; i++) {
                                get_broad_msg(msg, p, &dealer, i, num_ready, num_to_card);
                                write(num_fd_table[i], msg, strlen(msg));
                            }
                        }
                    } else if (buffer[0] == 'a') {
                        n = fd_num_table[fd];
                        sscanf(buffer, "action:(%s)", cmd);
                        token = strtok(cmd, "-");
                        hand_idx = 0;
                        while (token != NULL) {
                            if (strncmp(token, "split", 2) == 0) {
                                if (p[n].num_hands < 4)
                                    p[n].split(hand_idx);
                                else {
                                    p[n].hands[hand_idx].stand = 1;
                                    p[n].num_over++; 
                                }
                            } else if (strncmp(token, "double", 2) == 0) {
                                p[n].hands[hand_idx].is_double = 1;
                                p[n].add(deal(), hand_idx);
                                p[n].num_over++;
                            } else if (strncmp(token, "hit me", 2) == 0) {
                                p[n].add(deal(), hand_idx);
                                if (p[n].hands[hand_idx].get_score() > 21)
                                    p[n].num_over++;
                            } else if (strncmp(token, "stand", 2) == 0) {
                                p[n].hands[hand_idx].stand = 1;
                                p[n].num_over++;
                            }
                            token = strtok(NULL, "-");
                            hand_idx++;
                        }
                        get_player_msg(msg, p, &dealer, n, num_ready, num_to_card);
                        write(fd, msg, strlen(msg));
                    } else if (buffer[0] == 'f') {
                        num_finish++;
                        printf("player %d finished\n", fd_num_table[fd]);
                        if (num_finish == num_ready) {
                            dealer.do_dealer_job();
                            sprintf(msg, "dealer:");
                            for (i = 0; i < dealer.hands[0].num_cards; i++) {
                                sprintf(buffer, "%s ", num_to_card[dealer.hands[0].cards[i]]);
                                strcat(msg, buffer);
                            }
                            strcat(msg, "\n");
                            for (i = 0; i < num_ready; i++) {
                                strcpy(tmp_msg, msg);
                                if ((times = p[i].win_times(&dealer)) > 0) {
                                    sprintf(buffer, "win %d times.\n", times);
                                    strcat(tmp_msg, buffer);
                                }
                                else if (times == 0) {
                                    strcat(tmp_msg, "draw.\n");
                                } else { 
                                    sprintf(buffer, "lose 1 times.\n");
                                    strcat(tmp_msg, buffer);
                                }
                                write(num_fd_table[i], tmp_msg, strlen(tmp_msg));
                            }    
                            //clear player and dealer
                            for (i = 0; i < num_ready; i++)
                                p[i].clear();
                            dealer.clear();
                            //init params
                            num_ready = 0;
                            num_conn = 0;
                            game_played = 0;
                            //shuffle cards
                            srand(time(0));
                            random_shuffle(cards, cards + 52);
                            ///close connection
                            FD_ZERO(&active_fds);
                            FD_SET(listen_fd, &active_fds);
                            for (i = 0; i < num_conn; i++)
                                close(num_fd_table[i]);
                            printf("game over\n");
                        }
                    }
                }
            }
        }
    }
    //close connections
    for (fd = 3; fd < max_fd; fd++)
        if (FD_ISSET(fd, &active_fds))
            close(fd);
    return 0;
}
