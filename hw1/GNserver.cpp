#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <algorithm>

#define MAX_PLAYER 10

using namespace std;

//generate numbers to be guessed
void gen_num(int d[], int n[])
{
    int i;
    srand(time(0));
    random_shuffle(d, d + 10);
    for (i = 0; i < 4; i++)
        n[i] = d[i];
    return;
}

//if xAyB return 10x + y
int cmp_ans(int a[], int n[]) {
    int i = 0, j = 0, x = 0, y = 0;
    int is_a[4] = {0, 0, 0, 0};
    for (i = 0; i < 4; i++) {
        if (a[i] == n[i]) {
            x++;
            is_a[i] = 1;
        }
    }
    for (i = 0; i < 4; i++) {
        if (!is_a[i]) {
            for (j = 0; j < 4; j++) {
                if (!is_a[j]) {
                    if (n[i] == a[j])
                        y++;
                }
            }
        }
    }
    return 10 * x + y;
}

//decode number
void decode(int number, int numbers[])
{
    numbers[0] = number / 1000;
    number -= numbers[0] * 1000;
    numbers[1] = number / 100;
    number -= numbers[1] * 100;
    numbers[2] = number / 10;
    number -= numbers[2] * 10;
    numbers[3] = number;
    return;
}

//set is_win[i] = 1 if player[i] wins
void set_winners(int rounds[], int is_win[], int num_player)
{
    int n = 0;
    int i;
    int winners[MAX_PLAYER];
    int min_round = 10;
    for (i = 0; i < num_player; i++) {
        if (is_win[i]) {
            if (rounds[i] < min_round) {
                min_round = rounds[i];
                n = 0;
                winners[n] = i;
                n++;
            } else if (rounds[i] == min_round) {
                winners[n] = i;
                n++;
            }
        }
    }
    for (i = 0; i < num_player; i++)
        is_win[i] = 0;
    for (i = 0; i < n; i++)
        is_win[winners[i]] = 1;
    return;
}

int main(int argc, char *argv[])
{
    char buffer[100], msg[100];
    int listen_fd, conn_fd, max_fd;
    int i, fd;
    int number;
    int num_ready = 0, num_conn = 0, num_finish = 0;
    int game_played = 0;
    int digits[10];
    int ans[4], numbers[4], cmp;
    int rounds[MAX_PLAYER], is_win[MAX_PLAYER],  round = 0;
    int fd_num_table[20], num_fd_table[MAX_PLAYER];
    fd_set active_fds, read_fds;
    socklen_t length;
    struct sockaddr_in serverAddress, clientAddress;
    //set socket params
    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(atoi(argv[1]));
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(listen_fd, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
    //listen
    listen(listen_fd, 1);
    //init fd_set
    FD_ZERO(&active_fds);
    FD_SET(listen_fd, &active_fds);
    max_fd = listen_fd;
    //init digits
    for (i = 0; i < 10; i++)
        digits[i] = i;
    while(1) {
        read_fds = active_fds;
        select(max_fd + 1, &read_fds, NULL, NULL, NULL);
        for (fd = 3; fd <= max_fd; fd++) {
            if (FD_ISSET(fd, &read_fds)) {
                //new connection
                if (fd == listen_fd) {
                    length = sizeof(clientAddress);
                    conn_fd = accept(listen_fd, (struct sockaddr*)&clientAddress, &length);
                    //the room is playing
                    if (game_played || num_conn == MAX_PLAYER) {
                        sprintf(msg, "please wait for the next game\n");
                        write(conn_fd, msg, strlen(msg));
                        close(conn_fd);
                        continue;
                    }    
                    if (conn_fd == -1)
                        perror("accept");
                    else {
                        FD_SET(conn_fd, &active_fds);
                        fd_num_table[conn_fd] = num_conn;
                        num_fd_table[num_conn] = conn_fd;
                        printf("no.%d player entered\n", num_conn);
                        num_conn++;
                        if (conn_fd > max_fd)
                            max_fd = conn_fd;
                        sprintf(msg, "Welcome to guess number, enter r when you are ready. ");
                        write(conn_fd, msg, strlen(msg));
                    }
                } 
                //other connections
                else {
                    bzero(&buffer, sizeof(buffer));
                    read(fd, buffer, sizeof(buffer));
                    if (buffer[0] == 'r') { //ready
                        //init params
                        rounds[fd_num_table[fd]] = 0;
                        is_win[fd_num_table[fd]] = 0;
                        num_ready++;
                        printf("no.%d player is ready\n", fd_num_table[fd]);
                        //when everyone is ready
                        if (num_ready == num_conn) {
                            gen_num(digits, ans);
                            printf("ans:%d%d%d%d\n", ans[0], ans[1], ans[2], ans[3]);
                            game_played = 1;
                            num_finish = 0;
                            //sent game start msg to everyone
                            printf("game starts\n");
                            for (i = 0; i < num_ready; i++) {
                                sprintf(msg, "guess number starts\nplease enter your guess: ");
                                write(num_fd_table[i], msg, strlen(msg));
                            }
                        }
                    } else if (buffer[0] == 'g') { //guess number
                        rounds[fd_num_table[fd]]++;
                        round = rounds[fd_num_table[fd]];
                        sscanf(buffer, "guess:%04d", &number);
                        printf("no.%d player's guess is %04d\n", fd_num_table[fd], number);
                        decode(number, numbers);
                        cmp = cmp_ans(ans, numbers);
                        if (round < 10 && cmp != 40) {
                            sprintf(buffer, "round %d:%dA%dB\nplease enter your guess: ", round, cmp / 10, cmp % 10);
                            write(fd, buffer, strlen(buffer));
                        } else if (cmp == 40 || round == 10) {//4A0B or max_rounds
                            if (cmp == 40) {
                                is_win[fd_num_table[fd]] = 1;
                                sprintf(buffer, "correct at round %d, wait until others finish\n", round);
                            } else {
                                sprintf(buffer, "too many rounds, answer:%d%d%d%d, wait untill others finish\n", ans[0], ans[1], ans[2], ans[3]);
                            }
                            write(fd, buffer, strlen(buffer));
                        }
                    } else if (buffer[0] == 'f') {
                        num_finish++;
                        printf("%d players finished\n", num_finish);
                        if (num_finish == num_ready) {
                            //decide winners and reset is_win
                            set_winners(rounds, is_win, num_ready);
                            for (i = 0; i < num_ready; i++) {
                                if (is_win[i]) {
                                    sprintf(buffer, "win 100 dollars\n");
                                    write(num_fd_table[i], buffer, strlen(buffer));
                                }
                                else { 
                                    sprintf(buffer, "lose 100 dollars\n");
                                    write(num_fd_table[i], buffer, strlen(buffer));
                                }
                            }
                            //init params
                            num_ready = 0;
                            num_conn = 0;
                            game_played = 0;
                            ///close connection
                            FD_ZERO(&active_fds);
                            FD_SET(listen_fd, &active_fds);
                            for (i = 0; i < num_conn; i++)
                                close(num_fd_table[i]);
                            printf("game over\n");
                        }
                    }
                }
            }
        }
    }
    //close connections
    for (fd = 3; fd < max_fd; fd++)
        if (FD_ISSET(fd, &active_fds))
            close(fd);
    return 0;
}
